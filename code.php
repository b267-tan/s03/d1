<?php

// [SECTION] Objects as Variables
	// This is an example of "procedural approach"

$buildingObj = (object)[
	"name" => "Caswynn Building",
	"floors" => 8,
	"address" => "Brgy. Sacred Heart, Quezon City, Philippines"
];

$buildingObj2 = (object)[
	"name" => "GMA Network",
	"floors" => 20,
	"address" => "Brgy. Sacred Heart, Quezon City, Philippines"
];


// [SECTION] Ojbect from Classes
// hides the logic within the object and focuses more on the data structure within the program

class Building{
	
	// 1. Properties
	// Characteristics of an object
	public $name;
	public $floors;
	public $address;

	// 2. Constructor Function
		// A constructor allows us to initialize an class' properties upon creation/instantiation of an object.
		// double underscore
		public function __construct($name, $floors, $address){
			/*
				"$this" - refers to the properties and methods within the class scope.

				"$this->" is accessing the "name" property of the current class (Building) and assigning the value of the $name upon instantiation or creation of an object.
			*/

			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;
		}

		// 3. Methods
		// This are functions inside an object that can perform a specific action.
		public function printName(){
			return "The name of the building is $this->name";
		}
}

// Instantiating Building Class to create a new building object.
// "new" keyword is used to create an object from a class.
$building = new Building("Caswynn Building", 8, "Timog Avenue, Quezon City, Philippines");


// [SECTION] Inheritance and Polymorphism
	// Inheritance - the derived classes are allowed to inherit properties and methods from a specified base class.

Class Condominium extends Building{

	// Polymorphism - Methods inherited by the derived class can be overidden to have a behavior different from the method of the base class.

	public function printName(){
		return "The name of the condominium is $this->name";
	}

}

$condominium = new Condominium("Enzo Condo", 5, "Buendia Avenue, Makati City, Philippines");